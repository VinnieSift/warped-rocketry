package nebulaEngineers.warpedrocketry.stations;

//TODO: Make station work and be built on WD dimension.
//TODO: Attribute "created" had to be done again, has references in onModuleUnpack, beginTransition and getOrbitingPlanetId
//TODO: Attribute "dockingPoints" had to be done again, has references in onModuleUnpack, addDockingPosition and removeDockingPosition. TileDockingPort has to be warped.

import cr0s.warpdrive.config.WarpDriveConfig;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.DimensionManager;
import zmaster587.advancedRocketry.api.stations.IStorageChunk;
import zmaster587.advancedRocketry.stations.SpaceObject;
import zmaster587.advancedRocketry.tile.station.TileDockingPort;
import zmaster587.libVulpes.block.BlockFullyRotatable;
import zmaster587.libVulpes.util.HashedBlockPosition;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class WarpedSpaceObject extends SpaceObject {

	private boolean created;
	private HashMap<HashedBlockPosition, String> dockingPoints;
	private int orbitingPlanetId;

    public WarpedSpaceObject(){
        super();
		created = false;
		dockingPoints = new HashMap<HashedBlockPosition, String>();
    }

    @Override
	public void addDockingPosition(BlockPos pos, String str) {
		HashedBlockPosition pos2 = new HashedBlockPosition(pos);
		dockingPoints.put(pos2, str);
	}

	@Override
	public int getOrbitingPlanetId() {
    	int ret = -1;

    	if(created){
    		ret=this.orbitingPlanetId;
		}
    	return ret;
	}

	@Override
	public void removeDockingPosition(BlockPos pos) {
		HashedBlockPosition pos2 = new HashedBlockPosition(pos);
		dockingPoints.remove(pos2);
	}

	@Override
	/**
	 * WarpCores deactivated
	 */
	public boolean hasUsableWarpCore() {
		//return hasWarpCores && properties.getParentPlanet() != SpaceObjectManager.WARPDIMID && getDestOrbitingBody() != getOrbitingPlanetId();
		return false;
	}

	/**
	 * Useless without warpcore. Kept because of compatibility.
	 * @param time
	 */
	@Override
	public void beginTransition(long time) {

	}



    @Override
    public void onModuleUnpack(IStorageChunk chunk) {

		if(DimensionManager.isDimensionRegistered(WarpDriveConfig.G_SPACE_PROVIDER_ID) &&  DimensionManager.getWorld(WarpDriveConfig.G_SPACE_PROVIDER_ID) == null)
			DimensionManager.initDimension(WarpDriveConfig.G_SPACE_PROVIDER_ID);
		World worldObj = DimensionManager.getWorld(WarpDriveConfig.G_SPACE_PROVIDER_ID);
		if(!created) {
			chunk.pasteInWorld(worldObj, super.getSpawnLocation().x - chunk.getSizeX()/2, super.getSpawnLocation().y - chunk.getSizeY()/2, super.getSpawnLocation().z - chunk.getSizeZ()/2);

			created = true;
			//setLaunchPos((int)posX, (int)posZ);
			//setPos((int)posX, (int)posZ);
		}
		else {
			List<TileEntity> tiles = chunk.getTileEntityList();
			List<String> targetIds = new LinkedList<String>();
			List<TileEntity> myPoss = new LinkedList<TileEntity>();
			HashedBlockPosition pos = null;
			TileDockingPort destTile = null;
			TileDockingPort srcTile = null;

			//Iterate though all docking ports on the module in the chunk being launched
			for(TileEntity tile : tiles) {
				if(tile instanceof TileDockingPort) {
					targetIds.add(((TileDockingPort)tile).getTargetId());
					myPoss.add(tile);
				}
			}

			//Find the first docking port on the station that matches the id in the new chunk
			for(Map.Entry<HashedBlockPosition, String> map : dockingPoints.entrySet()) {
				if(targetIds.contains(map.getValue())) {
					int loc = targetIds.indexOf(map.getValue());
					pos = map.getKey();
					TileEntity tile;
					if((tile = worldObj.getTileEntity(pos.getBlockPos())) instanceof TileDockingPort) {
						destTile = (TileDockingPort)tile;
						srcTile = (TileDockingPort) myPoss.get(loc);
						break;
					}
				}
			}

			if(destTile != null) {
				EnumFacing stationFacing = destTile.getBlockType().getStateFromMeta(destTile.getBlockMetadata()).getValue(BlockFullyRotatable.FACING);
				EnumFacing moduleFacing = srcTile.getBlockType().getStateFromMeta(srcTile.getBlockMetadata()).getValue(BlockFullyRotatable.FACING);


				EnumFacing cross = moduleFacing.rotateAround(stationFacing.getAxis());

				if(stationFacing.getAxisDirection() == EnumFacing.AxisDirection.NEGATIVE)
					cross = cross.getOpposite();

				if(cross == moduleFacing) {
					if(moduleFacing == stationFacing) {
						if(cross == EnumFacing.DOWN || cross == EnumFacing.UP) {
							chunk.rotateBy(EnumFacing.NORTH);
							chunk.rotateBy(EnumFacing.NORTH);
						}
						else {
							chunk.rotateBy(EnumFacing.UP);
							chunk.rotateBy(EnumFacing.UP);
						}
					}
				}
				else if(cross.getOpposite() != moduleFacing)
					chunk.rotateBy(stationFacing.getFrontOffsetY() == 0 ? cross : cross.getOpposite());

				int xCoord = (stationFacing.getFrontOffsetX() == 0 ? -srcTile.getPos().getX() : srcTile.getPos().getX()*stationFacing.getFrontOffsetX()) + stationFacing.getFrontOffsetX() + destTile.getPos().getX();
				int yCoord = (stationFacing.getFrontOffsetY() == 0 ? -srcTile.getPos().getY() : srcTile.getPos().getY()*stationFacing.getFrontOffsetY()) + stationFacing.getFrontOffsetY() + destTile.getPos().getY();
				int zCoord = (stationFacing.getFrontOffsetZ() == 0 ? -srcTile.getPos().getZ() : srcTile.getPos().getZ()*stationFacing.getFrontOffsetZ()) + stationFacing.getFrontOffsetZ() + destTile.getPos().getZ();
				chunk.pasteInWorld(worldObj, xCoord, yCoord, zCoord);
				worldObj.setBlockToAir(destTile.getPos().offset(stationFacing));
				worldObj.setBlockToAir(destTile.getPos());
			}
		}
	}
}