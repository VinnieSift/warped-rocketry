package nebulaEngineers.warpedrocketry.entity;

//TODO: Make all verifications of Space dim in WD space 
//https://github.com/zmaster587/AdvancedRocketry/blob/b9733aaa946a6c4315a74b8e4d15a087dbff2324/src/main/java/zmaster587/advancedRocketry/entity/EntityRocket.java

import net.minecraft.world.World;
import zmaster587.advancedRocketry.api.StatsRocket;
import zmaster587.advancedRocketry.entity.EntityRocket;
import zmaster587.advancedRocketry.util.StorageChunk;

public class WarpedEntityRocket extends EntityRocket {


    public WarpedEntityRocket(World p_i1582_1_) {
        super(p_i1582_1_);
    }

    public WarpedEntityRocket(World world, StorageChunk storage, StatsRocket stats, double x, double y, double z) {
        super(world, storage, stats, x, y, z);
    }

    @Override
	public void onUpdate(){
        
    }

    @Override
    private void unpackSatellites(){

    }

    @Override
    public void launch() {

    }

    @Override
    public void onOrbitReached(){

    }
}