package nebulaEngineers.warpedrocketry;

import nebulaEngineers.warpedrocketry.tiles.WarpedTileStationBuilder;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import zmaster587.advancedRocketry.api.AdvancedRocketryItems;
import zmaster587.libVulpes.block.BlockTile;

@Mod(modid = "warpedrocketry",
        name = "Warped Rocketry",
        dependencies=""
                + "required-after:warpdrive;"
                + "required-after:advancedrocketry;"
)
public class WarpedRocketry{
    private static CreativeTabs tabWarRocketry = new CreativeTabs("warpedRocketry") {
        @Override
        public ItemStack getTabIconItem() {
            return new ItemStack(AdvancedRocketryItems.itemSpaceStationChip);
        }
    };

    
}
//TODO: Everything

